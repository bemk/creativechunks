package eu.orion_os.mc.creative_chunks.commands.gamemode;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import eu.orion_os.mc.creative_chunks.chunk_mode.EGameMode;
import eu.orion_os.mc.creative_chunks.constants.Constants;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.chunk.Chunk;

public class SetLocalGameModeRadius implements Command<ServerCommandSource>{

	final int MAX_RADIUS = 5;
	final EGameMode GAMEMODE;
	public SetLocalGameModeRadius(String gameMode) {
		this.GAMEMODE = EGameMode.fromNbt(gameMode);
	}

	@Override
	public int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
		int radius = IntegerArgumentType.getInteger(context, Constants.PARAM_RADIUS);
		if (radius < 1) {
			radius = 1;
		} else if (radius > MAX_RADIUS) {
			radius = MAX_RADIUS;
		}
		ServerCommandSource source = context.getSource();

		ServerWorld world = source.getPlayer().getServerWorld();
		ChunkPos centre = source.getPlayer().getChunkPos();

		for (int x = centre.x-radius; x <= centre.x+radius; x++) {
			for (int z = centre.z-radius; z <= centre.z+radius; z++) {
				ChunkPos chunkPos = new ChunkPos (x, z);
				BlockPos blockPos = chunkPos.getBlockPos(0, 0, 0);

				Chunk chunk = world.getChunk(blockPos);
				Constants.CHUNK_MODE_KEY.get(chunk).setChunkMode(GAMEMODE, world);
			}
		}

		return (radius+1) * (radius+1);
	}

}
