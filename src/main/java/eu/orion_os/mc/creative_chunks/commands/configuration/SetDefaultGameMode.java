package eu.orion_os.mc.creative_chunks.commands.configuration;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import eu.orion_os.mc.creative_chunks.chunk_mode.EGameMode;
import eu.orion_os.mc.creative_chunks.constants.Constants;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.LiteralText;

public class SetDefaultGameMode implements Command<ServerCommandSource> {

	@Override
	public int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
		ServerCommandSource source = context.getSource();
		String gameModeInput = StringArgumentType.getString(context, Constants.PARAM_GAME_MODE);

		boolean validParam = false;
		EGameMode requestedGameMode = EGameMode.DEFAULT;

		switch (gameModeInput) {
		case Constants.CHUNK_MODE_ADVENTURE:
			validParam = true;
			requestedGameMode = EGameMode.ADVENTURE;
			break;
		case Constants.CHUNK_MODE_CREATIVE:
			validParam = true;
			requestedGameMode = EGameMode.CREATIVE;
			break;
		case Constants.CHUNK_MODE_SPECTATOR:
			validParam = true;
			requestedGameMode = EGameMode.SPECTATOR;
			break;
		case Constants.CHUNK_MODE_SURVIVAL:
			validParam = true;
			requestedGameMode = EGameMode.SURVIVAL;
			break;
		default:
			break;
		}
		
		if (validParam) {
			Constants.WORLD_KEY.get(source.getPlayer().getServerWorld()).setDefaultGameMode(requestedGameMode);
			return 1;
		}
		source.sendError(new LiteralText("Unknown gamemode: " + gameModeInput));
		return 0;
	}

}
