package eu.orion_os.mc.creative_chunks.commands.gamemode;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import eu.orion_os.mc.creative_chunks.chunk_mode.EGameMode;
import eu.orion_os.mc.creative_chunks.constants.Constants;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.LiteralText;
import net.minecraft.text.TranslatableText;
import net.minecraft.world.chunk.Chunk;

public class SetLocalGameMode implements Command<ServerCommandSource> {

	@Override
	public int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
		ServerCommandSource source = context.getSource();
		source.sendError(new TranslatableText("/setChunkMode [default|survival|creative|spectator|adventure]"));
		return 0;
	}

	protected int setChunkMode(CommandContext<ServerCommandSource> context, String gameMode) throws CommandSyntaxException {
		ServerCommandSource source = context.getSource();
		ServerWorld world = source.getPlayer().getServerWorld();
		Chunk chunk = world.getChunk(source.getPlayer().getBlockPos());

		if (world.isClient) {
			source.sendError(new LiteralText("This command needs to run on the server"));
			return 0;
		}
		EGameMode chunkMode = EGameMode.fromNbt(gameMode);
		Constants.CHUNK_MODE_KEY.get(chunk).setChunkMode(chunkMode, world);

		source.sendFeedback(new LiteralText("Set chunk gamemode to " + chunkMode.toNbt()), true);
		return 1;
	}
}
