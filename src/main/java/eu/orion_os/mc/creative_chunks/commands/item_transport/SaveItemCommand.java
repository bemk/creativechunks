package eu.orion_os.mc.creative_chunks.commands.item_transport;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import eu.orion_os.mc.creative_chunks.constants.Constants;
import eu.orion_os.mc.creative_chunks.entity.IPlayer;
import eu.orion_os.mc.creative_chunks.world.IWorld;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.LiteralText;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

public class SaveItemCommand implements Command<ServerCommandSource> {

	@Override
	public int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
		PlayerEntity player = context.getSource().getPlayer();
		World world = player.getEntityWorld();
		IWorld worldSettings = Constants.WORLD_KEY.get(world);

		PlayerInventory inventory = player.getInventory();
		ItemStack toStore = inventory.getStack(inventory.selectedSlot);

		Identifier itemId = Registry.ITEM.getId(toStore.getItem());

		boolean storePermitted = worldSettings.isTransportAllowed(itemId);
		boolean itemNotAir = toStore.getItem() != Items.AIR;

		if (storePermitted && itemNotAir) {
			IPlayer playerStorage = Constants.PLAYER_KEY.get(player);
			playerStorage.addTransportItem(toStore);

			inventory.setStack(inventory.selectedSlot, new ItemStack(Items.AIR));
			return 1;
		} else if (!storePermitted && itemNotAir) {
			context.getSource().sendFeedback(new LiteralText("Game mode transporting of item " + itemId.toString() + " not permitted!"), false);
		}
		return 0;
	}

}
