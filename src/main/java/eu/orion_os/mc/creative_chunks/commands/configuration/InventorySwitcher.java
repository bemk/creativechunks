package eu.orion_os.mc.creative_chunks.commands.configuration;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import eu.orion_os.mc.creative_chunks.constants.Constants;
import eu.orion_os.mc.creative_chunks.world.IWorld;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.LiteralText;
import net.minecraft.world.World;

public class InventorySwitcher implements Command<ServerCommandSource>{

	@Override
	public int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
		ServerCommandSource source = context.getSource();
		World world = source.getWorld();
		IWorld worldSettings = Constants.WORLD_KEY.get(world);
		boolean switching = worldSettings.getInventorySwapping();
		
		try {
			worldSettings.setInventorySwapping(BoolArgumentType.getBool(context, Constants.PARAM_INVENTORY_SWITCHER));
		} catch (Exception e) {
			source.sendFeedback(new LiteralText("Switching: " + (switching ? "on" : "off")), false);
		}
		return 1;
	}

}
