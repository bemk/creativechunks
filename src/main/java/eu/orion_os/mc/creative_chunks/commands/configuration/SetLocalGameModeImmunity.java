package eu.orion_os.mc.creative_chunks.commands.configuration;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import eu.orion_os.mc.creative_chunks.constants.Constants;
import eu.orion_os.mc.creative_chunks.entity.IPlayer;
import net.minecraft.command.argument.EntityArgumentType;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;

public class SetLocalGameModeImmunity implements Command<ServerCommandSource> {

	@Override
	public int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
		ServerPlayerEntity player = EntityArgumentType.getPlayer(context, Constants.PARAM_PLAYER);
		boolean argument = BoolArgumentType.getBool(context, Constants.PARAM_IMMUNITY);
		IPlayer iPlayer = Constants.PLAYER_KEY.get(player);
		iPlayer.setLocalModeImmunity(argument);

		return 1;
	}

}
