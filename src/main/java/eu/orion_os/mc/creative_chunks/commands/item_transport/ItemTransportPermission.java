package eu.orion_os.mc.creative_chunks.commands.item_transport;


import com.mojang.brigadier.Command;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import eu.orion_os.mc.creative_chunks.constants.Constants;
import eu.orion_os.mc.creative_chunks.world.IWorld;
import net.minecraft.command.argument.ItemStackArgument;
import net.minecraft.command.argument.ItemStackArgumentType;
import net.minecraft.item.Item;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.LiteralText;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

public class ItemTransportPermission implements Command<ServerCommandSource> {

	@Override
	public int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
		World world = context.getSource().getWorld();
		IWorld worldSettings = Constants.WORLD_KEY.get(world);
		try {
			// Try setting the permission for the argument
			ItemStackArgument itemArgument = ItemStackArgumentType.getItemStackArgument(context, Constants.PARAM_ITEM_TYPE);
			Item item = itemArgument.getItem();
			Identifier id = Registry.ITEM.getId(item);

			boolean permission = BoolArgumentType.getBool(context, Constants.PARAM_PERMISSION);

			worldSettings.setTransportPermission(id, permission);
		} catch (Exception e) {
			// We were missing some form of argument. Show the list of permitted items instead.
			ServerCommandSource source = context.getSource();
			source.sendFeedback(new LiteralText("Items permitted for transport:"), false);
			for (Identifier i : worldSettings.transportPermittedItems()) {
				source.sendFeedback(new LiteralText("*" + i.toString()), false);
			}
		}
		return 1;
	}

}
