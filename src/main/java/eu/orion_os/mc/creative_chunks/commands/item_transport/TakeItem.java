package eu.orion_os.mc.creative_chunks.commands.item_transport;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import eu.orion_os.mc.creative_chunks.chunk_mode.EGameMode;
import eu.orion_os.mc.creative_chunks.constants.Constants;
import eu.orion_os.mc.creative_chunks.entity.IPlayer;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.server.command.ServerCommandSource;

public class TakeItem implements Command<ServerCommandSource> {

	@Override
	public int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
		PlayerEntity player = context.getSource().getPlayer();
		PlayerInventory inventory = player.getInventory();
		IPlayer ccPlayer = Constants.PLAYER_KEY.get(player);

		if (ccPlayer.getGameMode() == EGameMode.TRANSITION) {
			// Don't allow taking of items if they will get lost anyway
			return 0;
		}

		if (inventory.getStack(inventory.selectedSlot).getItem() == Items.AIR) {
			ItemStack stack = ccPlayer.takeTransportItem();
			if (stack != null) {
				inventory.setStack(inventory.selectedSlot, stack);
			}
		}
		return 0;
	}

}
