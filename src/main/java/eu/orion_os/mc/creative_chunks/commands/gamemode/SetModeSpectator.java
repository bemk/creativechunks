package eu.orion_os.mc.creative_chunks.commands.gamemode;

import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import eu.orion_os.mc.creative_chunks.constants.Constants;
import net.minecraft.server.command.ServerCommandSource;

public class SetModeSpectator extends SetLocalGameMode {

	@Override
	public int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
		return setChunkMode(context, Constants.CHUNK_MODE_SPECTATOR);
	}

}
