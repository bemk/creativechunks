package eu.orion_os.mc.creative_chunks.commands.gamemode;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import eu.orion_os.mc.creative_chunks.constants.Constants;
import eu.orion_os.mc.creative_chunks.entity.IPlayer;
import net.minecraft.command.argument.EntityArgumentType;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.LiteralText;

public class GetLocalGameModeImmunity implements Command<ServerCommandSource> {

	@Override
	public int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
		ServerPlayerEntity player = EntityArgumentType.getPlayer(context, Constants.PLAYER_ID);
		IPlayer iPlayer = Constants.PLAYER_KEY.get(player);

		boolean immune = iPlayer.getLocalModeImmunity();
		context.getSource().sendFeedback(new LiteralText("Immunity for [" + player.getName().asString() + "]: " + immune), false);

		return 1;
	}

}
