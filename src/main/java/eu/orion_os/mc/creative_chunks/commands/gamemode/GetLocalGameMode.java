package eu.orion_os.mc.creative_chunks.commands.gamemode;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import eu.orion_os.mc.creative_chunks.chunk_mode.EGameMode;
import eu.orion_os.mc.creative_chunks.constants.Constants;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.text.LiteralText;
import net.minecraft.world.chunk.Chunk;

public class GetLocalGameMode implements Command<ServerCommandSource>{

	@Override
	public int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
		ServerCommandSource source = context.getSource();

		ServerWorld world = source.getPlayer().getServerWorld();
		Chunk chunk = world.getChunk(source.getPlayer().getBlockPos());

		EGameMode chunkMode = Constants.CHUNK_MODE_KEY.get(chunk).getChunkMode();

		source.sendFeedback(new LiteralText("Local gamemode: " + chunkMode.toNbt()), false);

		return 1;
	}

}
