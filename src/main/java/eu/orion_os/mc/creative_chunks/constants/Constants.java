package eu.orion_os.mc.creative_chunks.constants;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dev.onyxstudios.cca.api.v3.component.ComponentKey;
import dev.onyxstudios.cca.api.v3.component.ComponentRegistry;
import eu.orion_os.mc.creative_chunks.chunk_mode.IChunkMode;
import eu.orion_os.mc.creative_chunks.entity.IInventorySwapper;
import eu.orion_os.mc.creative_chunks.entity.IPlayer;
import eu.orion_os.mc.creative_chunks.world.IWorld;
import net.minecraft.util.Identifier;

public final class Constants {
	public static final String MOD_ID 				= "creative_chunks";
	public static final String CHUNK_MODE_ID 		= "chunkmode";
	public static final String PLAYER_ID			= "player";
	public static final String WORLD_ID				= "world";
	public static final String DONKEY_ID			= "donkey";
	public static final String MINECART_ID			= "minecart";

	// NBT Constants
	public static final String TAG_CHUNK_MODE 			= "cc-mode";
	public static final String TAG_IMMUNITY 			= "localGameModeImmunity";
	public static final String TAG_INVENTORIES			= "inventories";
	public static final String TAG_INVENTORY_ADVENTURE 	= "adventureInventory";
	public static final String TAG_INVENTORY_CREATIVE	= "creativeInventory";
	public static final String TAG_INVENTORY_SPECTATOR	= "spectatorInventory";
	public static final String TAG_INVENTORY_SURVIVAL	= "survivalInventory";

	public static final String TAG_END_CHEST_ADVENTURE	= "adventureEndChest";
	public static final String TAG_END_CHEST_CREATIVE	= "creativeEndChest";
	public static final String TAG_END_CHEST_SPECTATOR	= "spectatorEndChest";
	public static final String TAG_END_CHEST_SURVIVAL	= "survivalEndChest";

	public static final String TAG_ITEM_TRANSPORT_QUEUE	= "itemTransportQueue";
	public static final String TAG_ITEM_SLOT			= "itemSlot";
	public static final String TAG_SWAPPING 			= "swapping";

	public static final String TAG_ITEM_TRANSPORT_PERMISSION 	= "transportPermission";

	public static final String CHUNK_MODE_DEFAULT 		= "default";
	public static final String CHUNK_MODE_CREATIVE 		= "creative";
	public static final String CHUNK_MODE_SURVIVAL 		= "survival";
	public static final String CHUNK_MODE_SPECTATOR 	= "spectator";
	public static final String CHUNK_MODE_ADVENTURE 	= "adventure";
	public static final String CHUNK_MODE_TRANSITION 	= "transition";

	// Command parameter constants
	public static final String PARAM_RADIUS					= "radius";
	public static final String PARAM_IMMUNITY				= "immunity";
	public static final String PARAM_PLAYER					= "player";
	public static final String PARAM_GAME_MODE				= "gamemode";
	public static final String PARAM_INVENTORY_SWITCHER 	= "inventory switching";
	public static final String PARAM_ITEM_TYPE				= "item";
	public static final String PARAM_PERMISSION				= "permission";

	// Cardinal Components registries
	public static final ComponentKey<IChunkMode> CHUNK_MODE_KEY = 
		ComponentRegistry.getOrCreate(
				new Identifier(MOD_ID, CHUNK_MODE_ID), IChunkMode.class);
	public static final ComponentKey<IPlayer> PLAYER_KEY = 
		ComponentRegistry.getOrCreate(
				new Identifier(MOD_ID, PLAYER_ID), IPlayer.class);
	public static final ComponentKey<IWorld> WORLD_KEY = 
		ComponentRegistry.getOrCreate(
				new Identifier(MOD_ID, WORLD_ID), IWorld.class);
	public static final ComponentKey<IInventorySwapper> DONKEY_KEY =
		ComponentRegistry.getOrCreate(
				new Identifier(MOD_ID, DONKEY_ID), IInventorySwapper.class);
	public static final ComponentKey<IInventorySwapper> MINECART_KEY =
		ComponentRegistry.getOrCreate(
				new Identifier(MOD_ID, MINECART_ID), IInventorySwapper.class);

	// Permission levels
	public static final int PERMISSION_LEVEL_ADMIN = 2;
	public static final int PERMISSION_LEVEL_PLAYER = 0;

	// Logger
	public static final Logger LOGGER = LogManager.getLogger(MOD_ID);
}
