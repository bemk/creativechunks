package eu.orion_os.mc.creative_chunks.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import eu.orion_os.mc.creative_chunks.constants.Constants;
import eu.orion_os.mc.creative_chunks.mixin_interfaces.IInventoryEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.vehicle.AbstractMinecartEntity;
import net.minecraft.entity.vehicle.StorageMinecartEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtElement;
import net.minecraft.nbt.NbtList;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.world.World;

@Mixin(StorageMinecartEntity.class)
public abstract class InventoryMinecartMixin extends AbstractMinecartEntity implements IInventoryEntity {

	protected InventoryMinecartMixin(EntityType<?> entityType, World world) {
		super(entityType, world);
	}
	
	@Shadow
	private DefaultedList<ItemStack> inventory;
	
	@Override
	public NbtList getInventory()
	{
		NbtList list = new NbtList();
		for (int i = 0; i < inventory.size(); i++) {
			NbtCompound compound = new NbtCompound();
			compound.putInt(Constants.TAG_ITEM_SLOT, i);
			inventory.get(i).writeNbt(compound);
			list.add(compound);
		}
		return list;
	}
	
	@Override
	public void setInventory(NbtList inventoryList)
	{
		inventory.clear();

		for (NbtElement i: inventoryList) {
			if (i instanceof NbtCompound) {
				NbtCompound stack = (NbtCompound)i;
				int slot = stack.getInt(Constants.TAG_ITEM_SLOT);
				if (slot < inventory.size()) {
					inventory.set(slot, ItemStack.fromNbt(stack));
				}
			}
		}
	}
	
	@Override
	public int inventorySize()
	{
		return inventory.size();
	}
}
