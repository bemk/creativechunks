package eu.orion_os.mc.creative_chunks.mixin;

import org.spongepowered.asm.mixin.Mixin;

import eu.orion_os.mc.creative_chunks.constants.Constants;
import eu.orion_os.mc.creative_chunks.mixin_interfaces.IInventoryEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.passive.AbstractDonkeyEntity;
import net.minecraft.entity.passive.HorseBaseEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtList;
import net.minecraft.world.World;

@Mixin(AbstractDonkeyEntity.class)
public abstract class DonkeyEntityMixin extends HorseBaseEntity implements IInventoryEntity {

	protected DonkeyEntityMixin(EntityType<? extends HorseBaseEntity> entityType, World world) {
		super(entityType, world);
	}

	@Override
	public NbtList getInventory()
	{
		NbtList nbtInventory = new NbtList();
		// Start at 1 to skip the saddle
		for (int i = 1; i < items.size(); i++) {
			ItemStack stack = items.getStack(i);
			if (!stack.isEmpty()) {
				NbtCompound compound = new NbtCompound();
				compound.putInt(Constants.TAG_ITEM_SLOT, i);
				stack.writeNbt(compound);
				nbtInventory.add(compound);
			}
		}

		return nbtInventory;
	}

	@Override
	public void setInventory(NbtList inventory)
	{
		// Clear all but the saddle
		ItemStack saddle = items.getStack(0);
		for (int i = 1; i < items.size(); i++) {
			items.removeStack(i);
		}

		items.setStack(0, saddle);
		for (int i = 0; i < inventory.size(); i++) {
			NbtCompound nbtItemStack = inventory.getCompound(i);
			int slot = nbtItemStack.getInt(Constants.TAG_ITEM_SLOT);
			items.setStack(slot, ItemStack.fromNbt(nbtItemStack));
		}
	}
	
	@Override
	public int inventorySize() {
		return getInventorySize();
	}
}
