package eu.orion_os.mc.creative_chunks.world;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import eu.orion_os.mc.creative_chunks.chunk_mode.EGameMode;
import eu.orion_os.mc.creative_chunks.constants.Constants;
import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtElement;
import net.minecraft.nbt.NbtList;
import net.minecraft.nbt.NbtString;
import net.minecraft.text.NbtText;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class World implements IWorld {
	private EGameMode defaultGameMode = EGameMode.SURVIVAL;
	private boolean inventorySwapping = true;
	private HashMap<Identifier, Boolean> allowedTransports = new HashMap<>();
	
	public World(net.minecraft.world.World world)
	{
	}

	@Override
	public void readFromNbt(NbtCompound tag) {
		if (tag.contains(Constants.TAG_CHUNK_MODE)) {
			defaultGameMode = EGameMode.fromId(tag.getInt(Constants.TAG_CHUNK_MODE));
			inventorySwapping = tag.getBoolean(Constants.TAG_SWAPPING);

			allowedTransports.clear();
			NbtList list = tag.getList(Constants.TAG_ITEM_TRANSPORT_PERMISSION, NbtType.STRING);
			for (NbtElement e : list) {
				NbtString stringId = (NbtString)e;
				Identifier id = new Identifier(stringId.asString());
				allowedTransports.put(id, true);
			}
		}
	}

	@Override
	public void writeToNbt(NbtCompound tag) {
		tag.putInt(Constants.TAG_CHUNK_MODE, defaultGameMode.getId());
		tag.putBoolean(Constants.TAG_SWAPPING, inventorySwapping);

		NbtList transportPermissions = new NbtList();
		for (Identifier i : allowedTransports.keySet()) {
			if (allowedTransports.get(i)) {
				String itemId = i.toString();
				transportPermissions.add(NbtString.of(itemId));
			}
		}
		tag.put(Constants.TAG_ITEM_TRANSPORT_PERMISSION, transportPermissions);
	}

	@Override
	public EGameMode getDefaultGameMode() {
		return defaultGameMode;
	}

	@Override
	public void setDefaultGameMode(EGameMode mode) {
		defaultGameMode = mode;
	}
	@Override
	public void setInventorySwapping(boolean swapping) {
		this.inventorySwapping = swapping;
	}
	@Override
	public boolean getInventorySwapping() {
		return inventorySwapping;
	}

	@Override
	public boolean isTransportAllowed(Identifier item) {
		if (allowedTransports.containsKey(item)) {
			return allowedTransports.get(item);
		}
		// Default deny
		return false;
	}

	@Override
	public void setTransportPermission(Identifier itemId, boolean permission) {
		allowedTransports.put(itemId, permission);
	}

	@Override
	public List<Identifier> transportPermittedItems() {
		ArrayList<Identifier> items = new ArrayList<>();
		for (Identifier i : allowedTransports.keySet()) {
			if (allowedTransports.get(i)) {
				items.add(i);
			}
		}

		return items;
	}

}
