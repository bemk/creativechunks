package eu.orion_os.mc.creative_chunks.world;

import dev.onyxstudios.cca.api.v3.world.WorldComponentFactoryRegistry;
import dev.onyxstudios.cca.api.v3.world.WorldComponentInitializer;
import eu.orion_os.mc.creative_chunks.constants.Constants;

public class WorldFactory implements WorldComponentInitializer {

	@Override
	public void registerWorldComponentFactories(WorldComponentFactoryRegistry registry) {
		registry.register(Constants.WORLD_KEY, (world) -> new World(world));
	}

}
