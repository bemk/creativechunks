package eu.orion_os.mc.creative_chunks.world;

import java.util.List;

import dev.onyxstudios.cca.api.v3.component.ComponentV3;
import eu.orion_os.mc.creative_chunks.chunk_mode.EGameMode;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;

public interface IWorld extends ComponentV3{
	public EGameMode getDefaultGameMode();
	public void setDefaultGameMode(EGameMode mode);
	public void setInventorySwapping(boolean swapping);
	public boolean getInventorySwapping();
	public boolean isTransportAllowed(Identifier item);
	public void setTransportPermission(Identifier itemId, boolean permission);
	public List<Identifier> transportPermittedItems();
}
