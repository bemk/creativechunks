package eu.orion_os.mc.creative_chunks.entity;

import java.util.LinkedList;
import java.util.Queue;

import dev.onyxstudios.cca.api.v3.component.tick.ServerTickingComponent;
import eu.orion_os.mc.creative_chunks.chunk_mode.EGameMode;
import eu.orion_os.mc.creative_chunks.constants.Constants;
import eu.orion_os.mc.creative_chunks.world.IWorld;
import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.EnderChestInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtElement;
import net.minecraft.nbt.NbtList;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.GameMode;
import net.minecraft.world.chunk.Chunk;

public class Player implements IPlayer, ServerTickingComponent {
	private final PlayerEntity player;

	private boolean localModeImmunity = false;
	private EGameMode gameMode = EGameMode.DEFAULT;

	private NbtList 
		adventureInventory = new NbtList(),
		creativeInventory = new NbtList(), 
		spectatorInventory = new NbtList(),
		survivalInventory = new NbtList();
	
	private NbtList
		adventureEndChest = new NbtList(),
		creativeEndChest = new NbtList(),
		spectatorEndChest = new NbtList(),
		survivalEndChest = new NbtList();

	private Queue<NbtCompound> itemTransportQueue = new LinkedList<>();

	public Player(PlayerEntity player)
	{
		this.player = player;
	}

	@Override
	public void readFromNbt(NbtCompound tag) {
		if (tag.contains(Constants.TAG_IMMUNITY)) {
			localModeImmunity = tag.getBoolean(Constants.TAG_IMMUNITY);

			adventureInventory = tag.getList(Constants.TAG_INVENTORY_ADVENTURE, NbtType.COMPOUND);
			creativeInventory = tag.getList(Constants.TAG_INVENTORY_CREATIVE, NbtType.COMPOUND);
			spectatorInventory = tag.getList(Constants.TAG_INVENTORY_SPECTATOR, NbtType.COMPOUND);
			survivalInventory = tag.getList(Constants.TAG_INVENTORY_SURVIVAL, NbtType.COMPOUND);

			adventureEndChest = tag.getList(Constants.TAG_END_CHEST_ADVENTURE, NbtType.COMPOUND);
			creativeEndChest = tag.getList(Constants.TAG_END_CHEST_CREATIVE, NbtType.COMPOUND);
			spectatorEndChest = tag.getList(Constants.TAG_END_CHEST_SPECTATOR, NbtType.COMPOUND);
			survivalEndChest = tag.getList(Constants.TAG_END_CHEST_SURVIVAL, NbtType.COMPOUND);

			gameMode = EGameMode.fromId(tag.getInt(Constants.TAG_CHUNK_MODE));

			NbtList itemQueue = tag.getList(Constants.TAG_ITEM_TRANSPORT_QUEUE, NbtType.COMPOUND);
			for (NbtElement e : itemQueue) {
				itemTransportQueue.add((NbtCompound)e);
			}
		} else {
			gameMode = EGameMode.convertGameMode(player.getServer().getDefaultGameMode());
		}
	}

	@Override
	public void writeToNbt(NbtCompound tag) {
		tag.putBoolean(Constants.TAG_IMMUNITY, localModeImmunity);

		tag.put(Constants.TAG_INVENTORY_ADVENTURE, adventureInventory);
		tag.put(Constants.TAG_INVENTORY_CREATIVE, creativeInventory);
		tag.put(Constants.TAG_INVENTORY_SPECTATOR, spectatorInventory);
		tag.put(Constants.TAG_INVENTORY_SURVIVAL, survivalInventory);

		tag.put(Constants.TAG_END_CHEST_ADVENTURE, adventureEndChest);
		tag.put(Constants.TAG_END_CHEST_CREATIVE, creativeEndChest);
		tag.put(Constants.TAG_END_CHEST_SPECTATOR, spectatorEndChest);
		tag.put(Constants.TAG_END_CHEST_SURVIVAL, survivalEndChest);

		tag.putInt(Constants.TAG_CHUNK_MODE, gameMode.getId());

		NbtList itemQueue = new NbtList();
		for (NbtCompound c : itemTransportQueue) {
			itemQueue.add(c);
		}
		tag.put(Constants.TAG_ITEM_TRANSPORT_QUEUE, itemQueue);
	}

	private boolean updateGamemode(EGameMode gameMode) {
		MinecraftServer server = this.player.getServer();
		String playerName = this.player.getName().asString();
		ServerPlayerEntity player = server.getPlayerManager().getPlayer(playerName);

		IWorld worldSettings = Constants.WORLD_KEY.get(player.getServerWorld());

		if (!localModeImmunity) {
			EGameMode previousMode = this.gameMode;
			this.gameMode = gameMode;

			GameMode nativeGameMode = gameMode.convertGameMode(gameMode, player.getServerWorld());
			if (nativeGameMode.isSurvivalLike()) {
				if (!landPlayer(player)) {
					// Don't switch game modes if the player can't be teleported to the ground.
					// They are locked inside a land mass, and will suffocate.
					// This can only happen with spectator mode
					return false;
				} else {
				}
			}

			if (worldSettings.getInventorySwapping()) {
				EGameMode defaultMode = worldSettings.getDefaultGameMode();
				// Catch default mode edge cases by just selecting the actual mode
				EGameMode selectedMode = (gameMode == EGameMode.DEFAULT) ? defaultMode : gameMode;
				previousMode = (previousMode == EGameMode.DEFAULT) ? defaultMode : previousMode;

				saveInventories(previousMode, player);
				setInventories(selectedMode);
			}

			player.changeGameMode(nativeGameMode);
		}
	
		return true;
	}

	private void saveInventories(EGameMode gameMode, ServerPlayerEntity player)
	{
		PlayerInventory inventory = player.getInventory();
		EnderChestInventory endChest = player.getEnderChestInventory();
		switch (gameMode) {
		case ADVENTURE:
			adventureInventory = inventory.writeNbt(new NbtList());
			adventureEndChest = endChest.toNbtList();
			break;
		case CREATIVE:
			creativeInventory = inventory.writeNbt(new NbtList());
			creativeEndChest = endChest.toNbtList();
			break;
		case SPECTATOR:
			spectatorInventory = inventory.writeNbt(new NbtList());
			spectatorEndChest = endChest.toNbtList();
			break;
		case SURVIVAL:
			survivalInventory = inventory.writeNbt(new NbtList());
			survivalEndChest = endChest.toNbtList();
			break;
		default:
			break;
		}
	}

	private void setInventories(EGameMode gameMode)
	{
		PlayerInventory inventory = player.getInventory();
		EnderChestInventory endChest = player.getEnderChestInventory();

		inventory.clear();
		endChest.clear();

		switch (gameMode) {
		case ADVENTURE:
			inventory.readNbt(adventureInventory);
			endChest.readNbtList(adventureEndChest);
			break;
		case CREATIVE:
			inventory.readNbt(creativeInventory);
			endChest.readNbtList(creativeEndChest);
			break;
		case SPECTATOR:
			inventory.readNbt(spectatorInventory);
			endChest.readNbtList(spectatorEndChest);
			break;
		case SURVIVAL:
			inventory.readNbt(survivalInventory);
			endChest.readNbtList(survivalEndChest);
			break;
		default:
			// Leave the inventory empty. We're probably transitioning
		}
	}

	// Find the nearest ground and put the player on top of that, to prevent them falling to their deaths.
	private boolean landPlayer(ServerPlayerEntity player)
	{
		BlockPos playerPosition = player.getBlockPos();
		if (player.getServerWorld().isWater(playerPosition)) {
			// The player can't fall if they're in water,
			// and there's time to react to drowning.
			return true;
		}
		ServerWorld world = player.getServerWorld();
		boolean teleported = false;

		// Search down for the nearest ground
		for (BlockPos position = player.getBlockPos(); !world.isOutOfHeightLimit(position) && !teleported; position = position.down()) {
			teleported = teleport(player, position);
		}
		
		// Search up for nearest ground
		for (BlockPos position = player.getBlockPos(); !world.isOutOfHeightLimit(position) && !teleported; position = position.up()) {
			teleported = teleport(player, position);
		}

		return teleported;
	}

	private boolean teleport(ServerPlayerEntity player, BlockPos test)
	{
		BlockPos oneUp = new BlockPos(test.getX(), test.getY()+1, test.getZ());
		boolean groundBlockPresent = !player.getServerWorld().isAir(test);
		boolean airBlockPresent = player.getServerWorld().isAir(oneUp);

		if (groundBlockPresent && airBlockPresent) {
			player.teleport(player.getX(), oneUp.getY(), player.getZ());
			player.fallDistance = 0f;
			return true;
		}
		return false;
	}

	@Override
	public void setLocalModeImmunity(boolean immunity) {
		this.localModeImmunity = immunity;
	}

	@Override
	public boolean getLocalModeImmunity() {
		return localModeImmunity;
	}

	@Override
	public void serverTick() {
		Chunk playerChunk = player.getEntityWorld().getChunk(player.getBlockPos());
		EGameMode chunkMode = Constants.CHUNK_MODE_KEY.get(playerChunk).getChunkMode();
		if (this.gameMode != chunkMode && !localModeImmunity) {
			updateGamemode(chunkMode);
		}

		if (gameMode == EGameMode.TRANSITION) {
			player.getInventory().clear();
			player.getEnderChestInventory().clear();
		}
	}

	@Override
	public void addTransportItem(ItemStack stack) {
		NbtCompound stackNbt = new NbtCompound();
		stack.writeNbt(stackNbt);
		itemTransportQueue.add(stackNbt);
	}

	@Override
	public ItemStack takeTransportItem() {
		return ItemStack.fromNbt(itemTransportQueue.poll());
	}

	@Override
	public EGameMode getGameMode() {
		return this.gameMode;
	}

}
