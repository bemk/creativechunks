package eu.orion_os.mc.creative_chunks.entity;

import dev.onyxstudios.cca.api.v3.entity.EntityComponentFactoryRegistry;
import dev.onyxstudios.cca.api.v3.entity.EntityComponentInitializer;
import dev.onyxstudios.cca.api.v3.entity.RespawnCopyStrategy;
import eu.orion_os.mc.creative_chunks.constants.Constants;
import net.minecraft.entity.passive.AbstractDonkeyEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.vehicle.StorageMinecartEntity;

public class EntityFactory implements EntityComponentInitializer {

	@Override
	public void registerEntityComponentFactories(EntityComponentFactoryRegistry registry) {
		// Swap inventories for players
		registry.beginRegistration(PlayerEntity.class, Constants.PLAYER_KEY)
			.respawnStrategy(RespawnCopyStrategy.ALWAYS_COPY)
			.impl(Player.class)
			.end(player -> new Player(player));

		// Swap inventories for donkey like entities
		registry.beginRegistration(AbstractDonkeyEntity.class, Constants.DONKEY_KEY)
			.impl(InventorySwapper.class)
			.end(donkey-> new InventorySwapper(donkey));

		// Swap inventories for minecarts with inventories
		registry.beginRegistration(StorageMinecartEntity.class, Constants.MINECART_KEY)
			.impl(InventorySwapper.class)
			.end(minecart -> new InventorySwapper(minecart));
	}

}
