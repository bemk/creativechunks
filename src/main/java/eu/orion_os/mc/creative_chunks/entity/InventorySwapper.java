package eu.orion_os.mc.creative_chunks.entity;

import dev.onyxstudios.cca.api.v3.component.tick.ServerTickingComponent;
import eu.orion_os.mc.creative_chunks.chunk_mode.EGameMode;
import eu.orion_os.mc.creative_chunks.chunk_mode.IChunkMode;
import eu.orion_os.mc.creative_chunks.constants.Constants;
import eu.orion_os.mc.creative_chunks.mixin_interfaces.IInventoryEntity;
import eu.orion_os.mc.creative_chunks.world.IWorld;
import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtList;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;

public class InventorySwapper implements ServerTickingComponent, IInventorySwapper {

	final protected Entity entity;
	final protected IInventoryEntity mixinEntity;
	// Allocate only once, saves time during game tick
	final private NbtList emptyList = new NbtList();

	protected EGameMode gameMode = EGameMode.DEFAULT;
	protected NbtList
		adventureInventory = new NbtList(),
		creativeInventory = new NbtList(),
		spectatorInventory = new NbtList(),
		survivalInventory = new NbtList();

	public InventorySwapper(Entity entity)
	{
		this.entity = entity;
		this.mixinEntity = (IInventoryEntity)entity;
	}

	@Override
	public void readFromNbt(NbtCompound tag) {
		if (tag.contains(Constants.TAG_CHUNK_MODE)) {
			gameMode = EGameMode.fromId(tag.getInt(Constants.TAG_CHUNK_MODE));
			adventureInventory = tag.getList(Constants.CHUNK_MODE_ADVENTURE, NbtType.COMPOUND);
			creativeInventory = tag.getList(Constants.CHUNK_MODE_CREATIVE, NbtType.COMPOUND);
			spectatorInventory = tag.getList(Constants.CHUNK_MODE_SPECTATOR, NbtType.COMPOUND);
			survivalInventory = tag.getList(Constants.CHUNK_MODE_SURVIVAL, NbtType.COMPOUND);
		}
	}

	@Override
	public void writeToNbt(NbtCompound tag) {
		tag.putInt(Constants.TAG_CHUNK_MODE, gameMode.getId());
		tag.put(Constants.CHUNK_MODE_ADVENTURE, adventureInventory);
		tag.put(Constants.CHUNK_MODE_CREATIVE, creativeInventory);
		tag.put(Constants.CHUNK_MODE_SPECTATOR, spectatorInventory);
		tag.put(Constants.CHUNK_MODE_SURVIVAL, survivalInventory);
	}
	
	protected void saveInventory(IInventoryEntity entity, EGameMode gameMode, EGameMode defaultMode)
	{
		NbtList nbtInventory = entity.getInventory();
		switch (gameMode) {
		case ADVENTURE:
			adventureInventory = nbtInventory;
			break;
		case CREATIVE:
			creativeInventory = nbtInventory;
			break;
		case SPECTATOR:
			spectatorInventory = nbtInventory;
			break;
		case SURVIVAL:
			survivalInventory = nbtInventory;
			break;
		case DEFAULT:
			saveInventory(entity, defaultMode, null);
			break;
		default:
			break;
		}
	}

	protected void setInventory(IInventoryEntity entity, EGameMode requested, EGameMode defaultMode)
	{
		NbtList selected = new NbtList();
		switch (requested) {
		case ADVENTURE:
			selected = adventureInventory;
			break;
		case CREATIVE:
			selected = creativeInventory;
			break;
		case SPECTATOR:
			selected = spectatorInventory;
			break;
		case SURVIVAL:
			selected = survivalInventory;
			break;
		case DEFAULT:
			setInventory(entity, defaultMode, null);
			return;
		default:
			break;
		}
		entity.setInventory(selected);
	}

	@Override
	public void serverTick() {
		World world = entity.getEntityWorld();
		IWorld worldSettings = Constants.WORLD_KEY.get(world);

		if (worldSettings.getInventorySwapping()) {
			Chunk chunk = world.getChunk(entity.getBlockPos());
			IChunkMode mode = Constants.CHUNK_MODE_KEY.get(chunk);
			EGameMode requested = mode.getChunkMode();
			if (this.gameMode != requested) {
				EGameMode defaultMode = Constants.WORLD_KEY.get(world).getDefaultGameMode();

				// swap inventories
				saveInventory(mixinEntity, this.gameMode, defaultMode);
				setInventory(mixinEntity, requested, defaultMode);
				this.gameMode = requested;
			}

			if (this.gameMode == EGameMode.TRANSITION) {
				mixinEntity.setInventory(emptyList);
			}
		}
	}

}
