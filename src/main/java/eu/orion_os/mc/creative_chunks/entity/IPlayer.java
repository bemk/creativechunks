package eu.orion_os.mc.creative_chunks.entity;

import dev.onyxstudios.cca.api.v3.component.ComponentV3;
import eu.orion_os.mc.creative_chunks.chunk_mode.EGameMode;
import net.minecraft.item.ItemStack;

public interface IPlayer extends ComponentV3 {
	public void setLocalModeImmunity(boolean immunity);
	public boolean getLocalModeImmunity();

	EGameMode getGameMode();

	public void addTransportItem(ItemStack stack);
	public ItemStack takeTransportItem();
}
