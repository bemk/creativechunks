package eu.orion_os.mc.creative_chunks.chunk_mode;

import dev.onyxstudios.cca.api.v3.chunk.ChunkComponentFactoryRegistry;
import dev.onyxstudios.cca.api.v3.chunk.ChunkComponentInitializer;
import eu.orion_os.mc.creative_chunks.constants.Constants;

public final class ChunkModesFactory implements ChunkComponentInitializer {

	@Override
	public void registerChunkComponentFactories(ChunkComponentFactoryRegistry registry) {
		registry.register(Constants.CHUNK_MODE_KEY, ChunkMode::new);
	}

}
