package eu.orion_os.mc.creative_chunks.chunk_mode;

import dev.onyxstudios.cca.api.v3.component.ComponentV3;
import net.minecraft.server.world.ServerWorld;

public interface IChunkMode extends ComponentV3 {

	public EGameMode getChunkMode();
	public void setChunkMode(EGameMode mode, ServerWorld world);
}
