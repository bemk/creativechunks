package eu.orion_os.mc.creative_chunks.chunk_mode;

import eu.orion_os.mc.creative_chunks.constants.Constants;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.ChunkPos;
import net.minecraft.world.chunk.Chunk;

public class ChunkMode implements IChunkMode {

	private final Chunk chunk;
	private EGameMode chunkMode = EGameMode.DEFAULT;
	
	public ChunkMode(Chunk chunk) {
		this.chunk = chunk;
	}
	
	@Override
	public void readFromNbt(NbtCompound tag) {
		if (tag.contains(Constants.TAG_CHUNK_MODE)) {
			chunkMode = EGameMode.fromNbt(tag.getString(Constants.TAG_CHUNK_MODE));
		}
	}

	@Override
	public void writeToNbt(NbtCompound tag) {
		tag.putString(Constants.TAG_CHUNK_MODE, chunkMode.toNbt());
	}

	@Override
	public EGameMode getChunkMode() {
		return chunkMode;
	}

	@Override
	public void setChunkMode(EGameMode mode, ServerWorld world) {
		chunkMode = mode;
		chunk.setShouldSave(true);

		ChunkPos myPos = chunk.getPos();
		// We need a transition layer around the creative chunks
		if (mode == EGameMode.CREATIVE) {

			// For every neighbour, if it is in default mode, set it to transition mode
			for (int x = -1; x <= 1; x++) {
				for (int z = -1; z <= 1; z++) {
					// Skip ourselves
					if (x == 0 && z == 0) {
						continue;
					}

					ChunkPos neighbourPos = new ChunkPos(myPos.x+x, myPos.z+z);
					Chunk neighbourChunk = world.getChunk(neighbourPos.getBlockPos(0, 0, 0));
					ChunkMode neighbourData = (ChunkMode) Constants.CHUNK_MODE_KEY.get(neighbourChunk);

					EGameMode neighbourMode = neighbourData.getChunkMode();
					if (neighbourMode == EGameMode.DEFAULT) {
						neighbourData.setChunkMode(EGameMode.TRANSITION, world);
					}
				}
			}
		}
		// Cleaning up of transitions has to be handled at the end of the command
		// Can't do that here, in the middle of resetting everything
	}

}
