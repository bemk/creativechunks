package eu.orion_os.mc.creative_chunks.chunk_mode;

import eu.orion_os.mc.creative_chunks.constants.Constants;
import net.minecraft.world.GameMode;
import net.minecraft.world.World;

public enum EGameMode {
	ADVENTURE(0),
	CREATIVE(1),
	DEFAULT(2),
	SPECTATOR(4),
	SURVIVAL(5),
	TRANSITION(6);

	private final int id;

	EGameMode(int id) {
		this.id = id;
	}

	public int getId() { return id; }
	
	public GameMode convertGameMode(EGameMode eGameMode, World world)
	{
		switch (eGameMode) {
		case ADVENTURE: 	return GameMode.ADVENTURE;
		case CREATIVE: 		return GameMode.CREATIVE;
		case DEFAULT: 		return convertGameMode(Constants.WORLD_KEY.get(world).getDefaultGameMode(), world);
		case SPECTATOR: 	return GameMode.SPECTATOR;
		case SURVIVAL: 		return GameMode.SURVIVAL;
		case TRANSITION: 	return GameMode.ADVENTURE;
		default: 	return convertGameMode(Constants.WORLD_KEY.get(world).getDefaultGameMode(), world);
		}
	}

	public static boolean modeMatchesTransition(EGameMode mode) {
		return (mode == TRANSITION || mode == ADVENTURE);
	}

	public static EGameMode fromId(int i)
	{
		switch (i) {
		case 0: return ADVENTURE;
		case 1: return CREATIVE;
		case 2: return DEFAULT;
		case 4: return SPECTATOR;
		case 5: return SURVIVAL;
		case 6: return TRANSITION;
		default: return DEFAULT;
		}
	}

	public static EGameMode convertGameMode(GameMode mode) 
	{
		switch(mode) {
		case ADVENTURE:
			return ADVENTURE;
		case CREATIVE:
			return CREATIVE;
		case SPECTATOR:
			return SPECTATOR;
		case SURVIVAL:
			return SURVIVAL;
		}
		return DEFAULT;
	}

	public static EGameMode fromNbt(String nbtChunkMode) {
		switch(nbtChunkMode) {
			case Constants.CHUNK_MODE_DEFAULT: 		return EGameMode.DEFAULT;
			case Constants.CHUNK_MODE_ADVENTURE: 	return EGameMode.ADVENTURE;
			case Constants.CHUNK_MODE_CREATIVE: 	return EGameMode.CREATIVE;
			case Constants.CHUNK_MODE_SPECTATOR: 	return EGameMode.SPECTATOR;
			case Constants.CHUNK_MODE_SURVIVAL: 	return EGameMode.SURVIVAL;
			case Constants.CHUNK_MODE_TRANSITION:  	return EGameMode.TRANSITION;
			default: 	return EGameMode.DEFAULT;
		}
	}
	
	public String toNbt() {
		switch (this) {
		case DEFAULT: 		return Constants.CHUNK_MODE_DEFAULT;
		case ADVENTURE: 	return Constants.CHUNK_MODE_ADVENTURE;
		case CREATIVE: 		return Constants.CHUNK_MODE_CREATIVE;
		case SPECTATOR:		return Constants.CHUNK_MODE_SPECTATOR;
		case SURVIVAL: 		return Constants.CHUNK_MODE_SURVIVAL;
		case TRANSITION:	return Constants.CHUNK_MODE_TRANSITION;
		}
		return Constants.CHUNK_MODE_DEFAULT;
	}
	
}
