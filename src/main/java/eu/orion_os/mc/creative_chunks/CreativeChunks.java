package eu.orion_os.mc.creative_chunks;

import com.mojang.brigadier.Command;
import com.mojang.brigadier.arguments.BoolArgumentType;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.arguments.StringArgumentType;
import com.mojang.brigadier.tree.LiteralCommandNode;

import eu.orion_os.mc.creative_chunks.chunk_mode.EGameMode;
import eu.orion_os.mc.creative_chunks.commands.configuration.InventorySwitcher;
import eu.orion_os.mc.creative_chunks.commands.configuration.SetDefaultGameMode;
import eu.orion_os.mc.creative_chunks.commands.configuration.SetLocalGameModeImmunity;
import eu.orion_os.mc.creative_chunks.commands.gamemode.GetLocalGameMode;
import eu.orion_os.mc.creative_chunks.commands.gamemode.GetLocalGameModeImmunity;
import eu.orion_os.mc.creative_chunks.commands.gamemode.GetLocalGameModeRadius;
import eu.orion_os.mc.creative_chunks.commands.gamemode.SetLocalGameMode;
import eu.orion_os.mc.creative_chunks.commands.gamemode.SetLocalGameModeRadius;
import eu.orion_os.mc.creative_chunks.commands.gamemode.SetModeAdventure;
import eu.orion_os.mc.creative_chunks.commands.gamemode.SetModeCreative;
import eu.orion_os.mc.creative_chunks.commands.gamemode.SetModeDefault;
import eu.orion_os.mc.creative_chunks.commands.gamemode.SetModeSpectator;
import eu.orion_os.mc.creative_chunks.commands.gamemode.SetModeSurvival;
import eu.orion_os.mc.creative_chunks.commands.item_transport.ItemTransportPermission;
import eu.orion_os.mc.creative_chunks.commands.item_transport.SaveItemCommand;
import eu.orion_os.mc.creative_chunks.commands.item_transport.TakeItem;
import eu.orion_os.mc.creative_chunks.constants.Constants;
import eu.orion_os.mc.creative_chunks.entity.IPlayer;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.command.v1.CommandRegistrationCallback;
import net.fabricmc.fabric.api.event.lifecycle.v1.ServerTickEvents;
import net.minecraft.command.argument.EntityArgumentType;
import net.minecraft.command.argument.ItemStackArgumentType;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.world.GameMode;
import net.minecraft.world.chunk.Chunk;

@SuppressWarnings("unused")
public class CreativeChunks implements ModInitializer {

	@Override
	public void onInitialize() {
		registerCommands();
	}

	private void registerCommands() 
	{
		CommandRegistrationCallback.EVENT.register((dispatcher, dedicated) -> {
			LiteralCommandNode<ServerCommandSource> setChunkMode = 
					CommandManager
						.literal("setLocalGameMode")
						.requires(source -> source.hasPermissionLevel(Constants.PERMISSION_LEVEL_ADMIN))
						.executes(new SetLocalGameMode())
						.build();

			LiteralCommandNode<ServerCommandSource> setModeDefault =
					CommandManager
						.literal("default")
						.executes(new SetModeDefault())
						.then(CommandManager.argument(Constants.PARAM_RADIUS, IntegerArgumentType.integer())
								.executes(new SetLocalGameModeRadius(Constants.CHUNK_MODE_DEFAULT)))
						.build();
			LiteralCommandNode<ServerCommandSource> setModeCreative =
					CommandManager
						.literal("creative")
						.executes(new SetModeCreative())
						.then(CommandManager.argument(Constants.PARAM_RADIUS, IntegerArgumentType.integer())
								.executes(new SetLocalGameModeRadius(Constants.CHUNK_MODE_CREATIVE)))
						.build();
			LiteralCommandNode<ServerCommandSource> setModeSurvival =
					CommandManager
						.literal("survival")
						.executes(new SetModeSurvival())
						.then(CommandManager.argument(Constants.PARAM_RADIUS, IntegerArgumentType.integer())
								.executes(new SetLocalGameModeRadius(Constants.CHUNK_MODE_SURVIVAL)))
						.build();
			LiteralCommandNode<ServerCommandSource> setModeSpectator =
					CommandManager
						.literal("spectator")
						.executes(new SetModeSpectator())
						.then(CommandManager.argument(Constants.PARAM_RADIUS, IntegerArgumentType.integer())
								.executes(new SetLocalGameModeRadius(Constants.CHUNK_MODE_SPECTATOR)))
						.build();
			LiteralCommandNode<ServerCommandSource> setModeAdventure =
					CommandManager
						.literal("adventure")
						.executes(new SetModeAdventure())
						.then(CommandManager.argument(Constants.PARAM_RADIUS, IntegerArgumentType.integer())
								.executes(new SetLocalGameModeRadius(Constants.CHUNK_MODE_ADVENTURE)))
						.build();
			LiteralCommandNode<ServerCommandSource> getChunkMode = 
					CommandManager
						.literal("getLocalGameMode")
						.executes(new GetLocalGameMode())
						.then(CommandManager.argument("radius", IntegerArgumentType.integer())
								.executes(new GetLocalGameModeRadius()))
						.build();
			LiteralCommandNode<ServerCommandSource> setLocalGameModeImmunity = 
					CommandManager
						.literal("setLocalGameModeImmunity")
						.requires(source -> source.hasPermissionLevel(Constants.PERMISSION_LEVEL_ADMIN))
						.then(CommandManager.argument(Constants.PARAM_PLAYER, EntityArgumentType.player())
							.then(CommandManager.argument(Constants.PARAM_IMMUNITY, BoolArgumentType.bool())
									.executes(new SetLocalGameModeImmunity())))
						.build();

			LiteralCommandNode<ServerCommandSource> getLocalGameModeImmunity =
					CommandManager
						.literal("getLocalGameModeImmunity")
						.requires(source -> source.hasPermissionLevel(Constants.PERMISSION_LEVEL_ADMIN))
						.then(CommandManager.argument(Constants.PARAM_PLAYER, EntityArgumentType.player())
								.executes(new GetLocalGameModeImmunity()))
						.build();

			LiteralCommandNode<ServerCommandSource> configureDefaultGameMode =
					CommandManager
						.literal("setDefaultGameMode")
						.requires(source -> source.hasPermissionLevel(Constants.PERMISSION_LEVEL_ADMIN))
						.then(CommandManager.argument(Constants.PARAM_GAME_MODE, StringArgumentType.string())
								.executes(new SetDefaultGameMode()))
						.build();
			LiteralCommandNode<ServerCommandSource> inventorySwitcher = 
					CommandManager
						.literal("inventorySwitching")
						.requires(source -> source.hasPermissionLevel(Constants.PERMISSION_LEVEL_ADMIN))
						.executes(new InventorySwitcher())
						.then(CommandManager.argument(Constants.PARAM_INVENTORY_SWITCHER, BoolArgumentType.bool())
								.executes(new InventorySwitcher()))
						.build();

			LiteralCommandNode<ServerCommandSource> saveItem =
					CommandManager
						.literal("saveItem")
						.executes(new SaveItemCommand())
						.build();
			LiteralCommandNode<ServerCommandSource> takeItem =
					CommandManager
						.literal("takeItem")
						.executes(new TakeItem())
						.build();
			LiteralCommandNode<ServerCommandSource> itemTransportPermission =
					CommandManager
						.literal("itemWhiteList")
						.requires(source -> source.hasPermissionLevel(Constants.PERMISSION_LEVEL_ADMIN))
						.executes(new ItemTransportPermission())
							.then(CommandManager.argument(Constants.PARAM_ITEM_TYPE, ItemStackArgumentType.itemStack())
									.then(CommandManager.argument(Constants.PARAM_PERMISSION, BoolArgumentType.bool())
											.executes(new ItemTransportPermission())))
						.build();

			setChunkMode.addChild(setModeAdventure);
			setChunkMode.addChild(setModeSpectator);
			setChunkMode.addChild(setModeSurvival);
			setChunkMode.addChild(setModeCreative);
			setChunkMode.addChild(setModeDefault);

			dispatcher.getRoot().addChild(setLocalGameModeImmunity);
			dispatcher.getRoot().addChild(getLocalGameModeImmunity);
			dispatcher.getRoot().addChild(getChunkMode);
			dispatcher.getRoot().addChild(setChunkMode);
			dispatcher.getRoot().addChild(getLocalGameModeImmunity);
			dispatcher.getRoot().addChild(configureDefaultGameMode);
			dispatcher.getRoot().addChild(inventorySwitcher);
			dispatcher.getRoot().addChild(saveItem);
			dispatcher.getRoot().addChild(takeItem);
			dispatcher.getRoot().addChild(itemTransportPermission);
		});
	}

}
