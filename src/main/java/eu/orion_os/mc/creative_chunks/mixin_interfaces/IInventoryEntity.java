package eu.orion_os.mc.creative_chunks.mixin_interfaces;

import net.minecraft.nbt.NbtList;

public interface IInventoryEntity {
	public NbtList getInventory();
	public void setInventory(NbtList inventory);

	public int inventorySize();
}
